var requirejs = require("requirejs");
var config = {
    appDir: "/var/www/lm.local/public/static/js/pack",
    baseUrl: ".",
    dir: "public/static/js/build",
    mainConfigFile: 'public/static/js/require/config.js'
};
requirejs.optimize(config, function(results) {
    console.log(results);
});