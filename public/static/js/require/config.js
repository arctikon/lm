requirejs.config({
    paths:{
        'jquery': 'jquery',
        'backbone': 'backbone',
        'marionette': 'backbone.marionette',
        'underscore': 'underscore'
    },
    shim: {
        'jquery': {
            exports: 'jQuery',
        },
        'underscore': {
            exports: '_'
        },		
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone'],
            exports: 'Backbone.Marionette'
        }
    }
});