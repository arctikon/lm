<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\Schema;

class CreateForumTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forumTopics', function($table) {
            $table->increments('id');
            $table->string('title')->default('')->comment('Заголовок');
            $table->integer('forumBoard_id')->default(0)->comment('id борда форума');
            $table->boolean('active')->default(0)->comment('Показывать?');
            $table->integer('sort')->default(0)->comment('Порядок');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forumTopics');
	}

}
