<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\Schema;

class CreateForumBoardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('forumBoards', function($table) {
            $table->increments('id');
            $table->string('title')->default('')->comment('Заголовок');
            $table->integer('forum_id')->default(0)->comment('id форума');
            $table->boolean('active')->default(0)->comment('Показывать?');
            $table->integer('sort')->default(0)->comment('Порядок');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forumBoards');
	}

}
