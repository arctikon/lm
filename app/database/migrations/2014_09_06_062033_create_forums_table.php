<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\Schema as Schema;

class CreateForumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('forums', function($table) {
            $table->increments('id');
            $table->string('name')->default('')->comment('Внутренее имя');
            $table->string('title')->default('')->comment('Заголовок');
            $table->boolean('active')->default(0)->comment('Показывать');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forums');
	}

}
