<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\Schema;

class CreateForumMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forumMessages', function($table) {
            $table->increments('id');
            $table->string('text')->default('')->comment('Текст');
            $table->string('author')->default('')->comment('Автор');
            $table->integer('forumTopic_id')->default(0)->comment('id темы форума');
            $table->boolean('active')->default(0)->comment('Показывать?');
            $table->integer('sort')->default(0)->comment('Порядок');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forumMessages');
	}

}
